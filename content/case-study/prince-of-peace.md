---
title: Prince of Peace Case Study
client: Price of Peace Lutheran Church
clientURL: https://www.princeofpeacecrestwood.com
feature: popl.jpg
subtitle: Revamping a local church website
description: A clean JAMstack build with a CMS
tags:
 - jamstack
 - mobile first
 - clean
 - minimal
 - branded
---

Hans Fiene is a funny guy.
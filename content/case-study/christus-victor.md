---
title: Christus Victor Case Study
client: Christus Victor Lutheran Church
clientURL: https://www.christusvictor.church
feature: cvlc.jpg
subtitle: For my own congregation
description: A quick and dirty JAMstack build for my church.
tags:
  - hugo
  - blog
  - youtube
  - cms
---

I'm probably going to be redoing this because I've learned a ton.
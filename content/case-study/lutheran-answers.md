---
title: Lutheran Answers Case Study
client: Lutheran Answers
clientURL: https://www.lutherananswers.com
feature: luan.jpg
subtitle: Documentation, Podcasting, Blogging
description: An indepth JAMstack challenging, with documentation, blogging, podcasting, and community built on a static platform.
tags:
  - documentation
  - hugo
  - Lunr.js
  - comments
  - podcast
---

This was my first project to use TAILWIND on.
---
title: West Storage Company Case Study
client: West Storage Company
clientURL: https://www.weststoragecompany.com
feature: west.jpg
subtitle: Putting a family business online
description: A challenging WordPress build with ease of use and current platform integration in mind.
tags:
  - WordPress
  - CMS
  - FontAwesome
  - Ease of Use
  - Inventory
  - Lead Generation
---

# A website that easily manages inventory

Carla West approached me via [Facebook Messenger](https://www.facebook.com/remy.sheppard.websites) and said she needed a new website for her business, **[West Storage Company](https://www.weststoragecompany.com)**. She needed something simple, clean, and professional.

**Further:** Mrs. West admitted both her and her husband could work Word, and were otherwise disinterested in learning some new technology.

-Needs Block Shortcode (don't forget to build this, you idiot.)-


{{< testimonial name="Carla West" position="Owner" >}}
  West Storage was in desperate need of a new website.  We looked at other places but Remy Sheppard was able to get me what I wanted and his vision was way better than mine.  When the virus hit our very best asset was that people were able to go right to our new website and see our current inventory available and the price of each building.  We have said over and over again what a great investment the website was for our small business.  It was a great decision then and still a great decision.  
{{< /testimonial >}}
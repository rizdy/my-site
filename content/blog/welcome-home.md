---
title: Welcome to the new website!
description: I spent quite a bit of time redeveloping my website on Hugo with TailwindCSS. Come on in and I'll give you a peak under the hood and talk about why I did things.
date: 2021-06-05
feature: welcome-home.jpg
category:
  - Web Dev
tags:
  - HUGO
  - HTML
  - SSG
  - Git
---

This is a blog post detailing my new website build on hugo and tailwind. I'm going to put some filler text in here now until I get around to writing it.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dignissim justo urna, et iaculis sapien ullamcorper id. Nunc rhoncus sapien non ex mollis convallis. Suspendisse non condimentum est. In imperdiet nunc sed enim convallis, et pretium est blandit. Nulla porttitor ante vel accumsan vehicula. Vestibulum maximus faucibus orci, et pretium erat lacinia et. In suscipit nisi in dui lobortis ultrices. Vivamus quis sodales nunc. Integer ac pretium nunc, ac hendrerit nunc.

In dapibus eros dui, non molestie nunc vestibulum eu. Nulla libero nisl, fringilla quis malesuada vitae, imperdiet ac mauris. Quisque gravida arcu ut nisl mollis, quis posuere nisl tempus. Maecenas cursus pharetra urna, vel commodo nunc consectetur mattis. Nam odio dui, interdum sed leo eget, ultrices vulputate arcu. Proin nisl magna, finibus nec pretium a, pellentesque vitae enim. Proin eu maximus nibh. Aliquam accumsan porttitor maximus. Sed hendrerit tempor eleifend. Praesent hendrerit venenatis justo, non auctor nisl porta nec. Etiam nec sem hendrerit, pharetra nisl nec, vulputate velit. Nam a est est. Praesent ac congue felis, posuere fringilla ante.

Curabitur sit amet nibh ultrices, elementum nunc et, vulputate diam. Aenean at odio aliquam, ullamcorper dolor vel, varius tellus. Etiam sit amet vestibulum est, ac ultricies neque. Suspendisse sagittis, mauris vel rutrum feugiat, ante metus consectetur nisi, nec tempus purus nulla eu ante. Proin elementum pulvinar fermentum. Etiam et lectus lacus. Nunc faucibus felis nec purus euismod blandit. Duis feugiat purus nisi, porta venenatis ante mattis ac.

Morbi bibendum sed risus id sodales. Donec in nibh efficitur, faucibus mi a, blandit tortor. Donec varius nunc nec lacinia facilisis. Aenean sodales, turpis eget porttitor accumsan, ante nibh accumsan mauris, sit amet hendrerit urna ipsum vitae quam. Pellentesque vel lacus nec libero vestibulum convallis. Ut elementum aliquam est sit amet elementum. Sed tincidunt, nunc eget tempor viverra, odio augue vestibulum ligula, nec lobortis nibh est in magna. Praesent quis commodo ipsum, dictum mattis eros. Cras in elementum lorem, non faucibus massa. Nunc quis nibh ullamcorper, facilisis leo non, laoreet nunc. Pellentesque dignissim elementum libero.

Ut eu sem et ligula semper imperdiet id condimentum mi. Cras id justo faucibus, vehicula erat eget, lobortis turpis. Sed vitae dolor id ligula maximus auctor sed egestas risus. Nullam consequat diam ac placerat condimentum. Proin mollis nisi nec tortor egestas, eget convallis eros luctus. Aliquam semper elit nulla, eu elementum tellus iaculis vitae. Mauris mattis lobortis nibh, id vestibulum ante varius eu. Quisque venenatis dolor quis odio dignissim hendrerit. Mauris augue tortor, facilisis et lectus id, pulvinar porta metus. Fusce ut mi quis arcu tincidunt eleifend. Proin at pharetra lectus. Suspendisse vitae egestas purus. Phasellus molestie eu metus eu venenatis. Ut ullamcorper odio nec tellus auctor eleifend.

Proin sodales lectus quis urna mattis, sed aliquam augue efficitur. Mauris vel tellus in lorem semper lobortis quis vel ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras at augue pretium, maximus risus ac, elementum diam. Cras eu diam velit. Vivamus dignissim leo maximus, rutrum dui non, ornare felis. Cras turpis lacus, feugiat vel mattis ut, tempus sit amet ex. Integer dictum imperdiet massa eget molestie.

Donec mollis mauris eget ante aliquet, dapibus pulvinar purus maximus. Vivamus ullamcorper purus mi, eget congue leo posuere vitae. In tempus dui vel nisi ultrices aliquet. Ut ac leo dapibus, accumsan ante in, sagittis erat. Curabitur pulvinar eget enim vitae volutpat. Nulla pulvinar nisl pulvinar scelerisque ullamcorper. Quisque maximus diam ac elit ornare sollicitudin.

Aliquam erat volutpat. Sed sapien ipsum, auctor sit amet nunc sit amet, eleifend consectetur arcu. Nulla lacinia congue nisi in vulputate. Aliquam laoreet cursus hendrerit. Cras eleifend erat eros, a blandit lacus vehicula vitae. Proin non hendrerit magna, id fringilla arcu. Maecenas pharetra varius sapien non malesuada. Maecenas leo odio, sollicitudin eu dapibus nec, euismod eu arcu. Aenean ultrices est et dui ullamcorper, nec lobortis ante eleifend. Sed maximus lobortis justo, sit amet egestas risus imperdiet in. Nullam id scelerisque quam. Nulla accumsan scelerisque blandit. Cras pretium odio justo. Nam pulvinar tortor arcu, blandit rhoncus neque dapibus vel.

Fusce pretium nulla nec ipsum facilisis tincidunt. Etiam viverra tortor nec nisl fermentum, eget pulvinar sem sodales. Maecenas auctor lacus nec pretium tristique. Quisque imperdiet orci commodo augue scelerisque dapibus. Proin iaculis est leo, eu feugiat nisl efficitur quis. Sed mollis, orci at hendrerit commodo, leo arcu molestie enim, sit amet pharetra odio sapien et lectus. Vivamus nec elementum ante, ac vehicula turpis. Donec ullamcorper, sem vel iaculis iaculis, augue leo laoreet ante, congue semper nibh lorem lobortis nisi. Curabitur eleifend imperdiet tellus, in egestas lorem ullamcorper non. Phasellus nulla sapien, tincidunt eu orci eget, eleifend eleifend ipsum. Donec nec malesuada nunc, sit amet pretium ipsum. Suspendisse maximus aliquam ipsum vel sagittis.

Nunc tempus pretium metus sed condimentum. Vivamus diam leo, condimentum ut mauris vitae, varius mollis ipsum. Aenean venenatis eget dolor in ultricies. Suspendisse lectus mauris, pellentesque vel dolor ac, porta congue nibh. Morbi ac dictum ipsum. In condimentum et nulla quis dapibus. Duis lacus quam, fringilla at consectetur sit amet, vulputate id purus. Proin sed accumsan ex, suscipit consequat diam.

**Generated 10 paragraphs, 836 words, 5656 bytes of [Lorem Ipsum](https://www.lipsum.com/)**
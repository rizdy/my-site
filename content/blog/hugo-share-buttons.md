---
title: "Creating Social Share Buttons for Hugo"
date: 2021-06-20T09:28:33-04:00
description: I needed some social share buttons for my Hugo build of Lutheran Answers, but everyone's share buttons suck. So I made my own with Tailwind
feature: social-media.jpg
category:
  - Hugo
  - Development
tags:
  - Hugo
  - Tailwind CSS
  - Social Media
  - Sharing
---

I wanted some nice, super easy share buttons for [Lutheran Answers](https://www.lutherananswers.com) that didn't break the pagespeed bank. 

The problem with share buttons, though, is that everyone wants fancy functionality. They want share counts, upvoting, etc but they don't actually know how to code. 

To solve this problem, Facebook, Twitter, and everyone else builds their own buttons.

But with those buttons come SDKs, giant JavaScript imports, CSS links, and SVG images, tracking snippets, cookies, and everything else that slows down your page speed.

And getting between 95 and 100 on Pagespeed Insights is super important to me! That's 50% of the draw of building sites with Hugo and other SSGs: The near-instant load times.

So instead of doing the copy/paste thing from Facebook Developers or whatever, I decided to forgo the need for fancy post count functionality and build my own simple buttons:

![Finished share buttons!](/images/blog/hugo-share-buttons/buttons-finished.jpg)

## Step One

Step number one was to grab all of the social share links and add `GoLang` placeholders:

```html
https://www.facebook.com/sharer.php?u={{ .Permalink }}
https://twitter.com/share?url={{ .Permalink }}&text={{ .Title }}&via=@lutherananswers
https://www.linkedin.com/shareArticle?url={{ .Permalink }}&title={{ .Title }}
https://www.tumblr.com/share/link?url={{ .Permalink }}&name={{ .Title }}&description=[post-desc]
https://reddit.com/submit?url={{ .Permalink }}&title={{ .Title }}
https://www.stumbleupon.com/submit?url={{ .Permalink }}&title={{ .Title }}
https://getpocket.com/save?url={{ .Permalink }}&title={{ .Title }}
mailto:?subject={{ .Title }}&body=Check out this article on Lutheran Answers: {{ .Permalink }}
```

Easy enough.

## Step Two: Build the share link

Now I needed to build the actual share link. I came up with the following:

```html
<a href="" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-brand border-transparent hover:text-brand hover:border-brand hover:bg-white">
  SVG
</a>
```

I kept this at the bottom of the template while I worked so I could just copy+paste it without having to re-write it every time. I could then do a search+replace for `brand` to achieve the colors.

### That Reminds Me: Brand Colors

For the brand colors I'm using the Tailwinds Plugin "[TailwindCSS Brand Colors](https://github.com/praveenjuge/tailwindcss-brand-colors)" by [Praveen Juge](https://praveenjuge.com/).

## Step Three: Get the SVGs

I just downloaded the FontAwesome SVGs for each brand icon and then ripped the source. I cleared out all the code that wasn't strictly necessary. I've got the SVGs below for you, just make sure you credit FontAwesome! They do amazing work.

#### Facebook
```html
<svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="h-6 w-6" viewBox="0 0 24 24">
  <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
</svg>
```

#### Twitter
```html
<svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="h-5 w-5" viewBox="0 0 24 24">
  <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
</svg>
```

#### Reddit
```html
<svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
  <path fill="currentColor" d="M440.3 203.5c-15 0-28.2 6.2-37.9 15.9-35.7-24.7-83.8-40.6-137.1-42.3L293 52.3l88.2 19.8c0 21.6 17.6 39.2 39.2 39.2 22 0 39.7-18.1 39.7-39.7s-17.6-39.7-39.7-39.7c-15.4 0-28.7 9.3-35.3 22l-97.4-21.6c-4.9-1.3-9.7 2.2-11 7.1L246.3 177c-52.9 2.2-100.5 18.1-136.3 42.8-9.7-10.1-23.4-16.3-38.4-16.3-55.6 0-73.8 74.6-22.9 100.1-1.8 7.9-2.6 16.3-2.6 24.7 0 83.8 94.4 151.7 210.3 151.7 116.4 0 210.8-67.9 210.8-151.7 0-8.4-.9-17.2-3.1-25.1 49.9-25.6 31.5-99.7-23.8-99.7zM129.4 308.9c0-22 17.6-39.7 39.7-39.7 21.6 0 39.2 17.6 39.2 39.7 0 21.6-17.6 39.2-39.2 39.2-22 .1-39.7-17.6-39.7-39.2zm214.3 93.5c-36.4 36.4-139.1 36.4-175.5 0-4-3.5-4-9.7 0-13.7 3.5-3.5 9.7-3.5 13.2 0 27.8 28.5 120 29 149 0 3.5-3.5 9.7-3.5 13.2 0 4.1 4 4.1 10.2.1 13.7zm-.8-54.2c-21.6 0-39.2-17.6-39.2-39.2 0-22 17.6-39.7 39.2-39.7 22 0 39.7 17.6 39.7 39.7-.1 21.5-17.7 39.2-39.7 39.2z"></path>
</svg>
```

#### StumbleUpon
```html
<svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
  <path fill="currentColor" d="M502.9 266v69.7c0 62.1-50.3 112.4-112.4 112.4-61.8 0-112.4-49.8-112.4-111.3v-70.2l34.3 16 51.1-15.2V338c0 14.7 12 26.5 26.7 26.5S417 352.7 417 338v-72h85.9zm-224.7-58.2l34.3 16 51.1-15.2V173c0-60.5-51.1-109-112.1-109-60.8 0-112.1 48.2-112.1 108.2v162.4c0 14.9-12 26.7-26.7 26.7S86 349.5 86 334.6V266H0v69.7C0 397.7 50.3 448 112.4 448c61.6 0 112.4-49.5 112.4-110.8V176.9c0-14.7 12-26.7 26.7-26.7s26.7 12 26.7 26.7v30.9z"></path>
</svg>
```
#### tumblr
```html
<svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
  <path fill="currentColor" d="M309.8 480.3c-13.6 14.5-50 31.7-97.4 31.7-120.8 0-147-88.8-147-140.6v-144H17.9c-5.5 0-10-4.5-10-10v-68c0-7.2 4.5-13.6 11.3-16 62-21.8 81.5-76 84.3-117.1.8-11 6.5-16.3 16.1-16.3h70.9c5.5 0 10 4.5 10 10v115.2h83c5.5 0 10 4.4 10 9.9v81.7c0 5.5-4.5 10-10 10h-83.4V360c0 34.2 23.7 53.6 68 35.8 4.8-1.9 9-3.2 12.7-2.2 3.5.9 5.8 3.4 7.4 7.9l22 64.3c1.8 5 3.3 10.6-.4 14.5z"></path>
</svg>
```

#### Pocket
```html
<svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
  <path fill="currentColor" d="M407.6 64h-367C18.5 64 0 82.5 0 104.6v135.2C0 364.5 99.7 464 224.2 464c124 0 223.8-99.5 223.8-224.2V104.6c0-22.4-17.7-40.6-40.4-40.6zm-162 268.5c-12.4 11.8-31.4 11.1-42.4 0C89.5 223.6 88.3 227.4 88.3 209.3c0-16.9 13.8-30.7 30.7-30.7 17 0 16.1 3.8 105.2 89.3 90.6-86.9 88.6-89.3 105.5-89.3 16.9 0 30.7 13.8 30.7 30.7 0 17.8-2.9 15.7-114.8 123.2z"></path>
</svg>
```

#### LinkedIn
```html
<svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
  <path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
</svg>
```

#### Paper Plane
```html
<svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
  <path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path>
</svg>
```

## Putting it all together

The last step was slam the proper SVG into the link, copy+paste the hypertext reference, and we had some awesome social share buttons! Here they are again, along with what it looks like when you hover them:

![Finished share buttons](/images/blog/hugo-share-buttons/buttons-finished.jpg)
![Finished share buttons - hover](/images/blog/hugo-share-buttons/finished-hover.jpg)

Here's the whole code as a partial, available at the [Lutheran Answers Git Repo](https://gitlab.com/rizdy/luanre/-/blob/master/layouts/partials/social-share.html):

```html
<h3 class="mb-3">Please Consider Sharing</h3>
<span class="flex flex-row">

  <a href="https://www.facebook.com/sharer.php?u={{ .Permalink }}" target="_blank" class=" inline-flex mr-5 flex-row item-center rounded-lg p-2 border transition-all duration-200 text-white bg-facebook border-transparent hover:text-facebook hover:border-facebook hover:bg-white shadow">
    <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="h-6 w-6" viewBox="0 0 24 24">
      <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
    </svg>
    &nbsp;Facebook
  </a>

  <a href="https://twitter.com/share?url={{ .Permalink }}&text={{ .Title }}&via=@lutherananswers" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-twitter border-transparent hover:text-twitter hover:border-twitter hover:bg-white shadow">
    <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="h-5 w-5" viewBox="0 0 24 24">
      <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
    </svg>
    &nbsp;Twitter
  </a>

  <a href="https://reddit.com/submit?url={{ .Permalink }}&title={{ .Title }}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-reddit border-transparent hover:text-reddit hover:border-reddit hover:bg-white shadow">
    <svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path fill="currentColor" d="M440.3 203.5c-15 0-28.2 6.2-37.9 15.9-35.7-24.7-83.8-40.6-137.1-42.3L293 52.3l88.2 19.8c0 21.6 17.6 39.2 39.2 39.2 22 0 39.7-18.1 39.7-39.7s-17.6-39.7-39.7-39.7c-15.4 0-28.7 9.3-35.3 22l-97.4-21.6c-4.9-1.3-9.7 2.2-11 7.1L246.3 177c-52.9 2.2-100.5 18.1-136.3 42.8-9.7-10.1-23.4-16.3-38.4-16.3-55.6 0-73.8 74.6-22.9 100.1-1.8 7.9-2.6 16.3-2.6 24.7 0 83.8 94.4 151.7 210.3 151.7 116.4 0 210.8-67.9 210.8-151.7 0-8.4-.9-17.2-3.1-25.1 49.9-25.6 31.5-99.7-23.8-99.7zM129.4 308.9c0-22 17.6-39.7 39.7-39.7 21.6 0 39.2 17.6 39.2 39.7 0 21.6-17.6 39.2-39.2 39.2-22 .1-39.7-17.6-39.7-39.2zm214.3 93.5c-36.4 36.4-139.1 36.4-175.5 0-4-3.5-4-9.7 0-13.7 3.5-3.5 9.7-3.5 13.2 0 27.8 28.5 120 29 149 0 3.5-3.5 9.7-3.5 13.2 0 4.1 4 4.1 10.2.1 13.7zm-.8-54.2c-21.6 0-39.2-17.6-39.2-39.2 0-22 17.6-39.7 39.2-39.7 22 0 39.7 17.6 39.7 39.7-.1 21.5-17.7 39.2-39.7 39.2z"></path>
    </svg>
    &nbsp;Reddit
  </a>

  <a href="https://www.stumbleupon.com/submit?url={{ .Permalink }}&title={{ .Title }}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-red-600 border-transparent hover:text-red-600 hover:border-red-600 hover:bg-white shadow">
    <svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path fill="currentColor" d="M502.9 266v69.7c0 62.1-50.3 112.4-112.4 112.4-61.8 0-112.4-49.8-112.4-111.3v-70.2l34.3 16 51.1-15.2V338c0 14.7 12 26.5 26.7 26.5S417 352.7 417 338v-72h85.9zm-224.7-58.2l34.3 16 51.1-15.2V173c0-60.5-51.1-109-112.1-109-60.8 0-112.1 48.2-112.1 108.2v162.4c0 14.9-12 26.7-26.7 26.7S86 349.5 86 334.6V266H0v69.7C0 397.7 50.3 448 112.4 448c61.6 0 112.4-49.5 112.4-110.8V176.9c0-14.7 12-26.7 26.7-26.7s26.7 12 26.7 26.7v30.9z"></path>
    </svg>
  </a>

  <a href="https://www.tumblr.com/share/link?url={{ .Permalink }}&name={{ .Title }}{{- with .Params.description -}}&description={{- . -}}{{- end -}}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-tumblr border-transparent hover:text-tumblr hover:border-tumblr hover:bg-white shadow">
    <svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
      <path fill="currentColor" d="M309.8 480.3c-13.6 14.5-50 31.7-97.4 31.7-120.8 0-147-88.8-147-140.6v-144H17.9c-5.5 0-10-4.5-10-10v-68c0-7.2 4.5-13.6 11.3-16 62-21.8 81.5-76 84.3-117.1.8-11 6.5-16.3 16.1-16.3h70.9c5.5 0 10 4.5 10 10v115.2h83c5.5 0 10 4.4 10 9.9v81.7c0 5.5-4.5 10-10 10h-83.4V360c0 34.2 23.7 53.6 68 35.8 4.8-1.9 9-3.2 12.7-2.2 3.5.9 5.8 3.4 7.4 7.9l22 64.3c1.8 5 3.3 10.6-.4 14.5z"></path>
    </svg>
  </a>

  <a href="https://getpocket.com/save?url={{ .Permalink }}&title={{ .Title }}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-pocket border-transparent hover:text-pocket hover:border-pocket hover:bg-white shadow">
    <svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
      <path fill="currentColor" d="M407.6 64h-367C18.5 64 0 82.5 0 104.6v135.2C0 364.5 99.7 464 224.2 464c124 0 223.8-99.5 223.8-224.2V104.6c0-22.4-17.7-40.6-40.4-40.6zm-162 268.5c-12.4 11.8-31.4 11.1-42.4 0C89.5 223.6 88.3 227.4 88.3 209.3c0-16.9 13.8-30.7 30.7-30.7 17 0 16.1 3.8 105.2 89.3 90.6-86.9 88.6-89.3 105.5-89.3 16.9 0 30.7 13.8 30.7 30.7 0 17.8-2.9 15.7-114.8 123.2z"></path>
    </svg>
  </a>

  <a href="https://www.linkedin.com/shareArticle?url={{ .Permalink }}&title={{ .Title }}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-linkedin border-transparent hover:text-linkedin hover:border-linkedin hover:bg-white shadow">
    <svg class="w-5 h-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
      <path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
    </svg>
  </a>

  <a href="mailto:?subject={{ .Title }}&body=Check out this article on Lutheran Answers: {{ .Permalink }}" target="_blank" class="inline-flex flex-row items-center border rounded-lg p-2 mr-5 transition-all duration-200 text-white bg-yellow-400 border-transparent hover:text-yellow-400 hover:border-yellow-400 hover:bg-white">
    <svg class="h-5 w-5" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path>
    </svg>
  </a>

</span>
```

## Closing Thoughts

I'm sure you could `DRY` this out some more, maybe with a `{{ range }}` of some kind. I'm honestly not too concerned with trying to figure it out, though.

Hope this helps you build your own social share buttons! Or feel free to just rip mine. 'S'all good!
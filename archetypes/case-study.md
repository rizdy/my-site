---
title: "{{ replace .Name "-" " " | title }}"
subtitle: A pithy little sentence!
description: A short sentence or two about the design, its challenges, and what processes we used to overcome them. 
date: {{ .Date }}
client: John's Services, LLC
clientURL: JohnsWebsite.org
feature: JohnsPic.jpg
tags:
  - A list
---

Doing up some content, family!